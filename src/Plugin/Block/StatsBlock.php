<?php

namespace Drupal\abc_stats\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\abc_stats\Counter;
use Drupal\Core\Cache\Cache;

/**
 * Provides a statistics block.
 *
 * @Block(
 *   id = "abc_stats_block",
 *   admin_label = @Translation("Statistics"),
 * )
 */
class StatsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build () {
    $container = [
        '#type' => 'container',
    ];
    $this->addStats($container, $this->t('Users'),
        (new Counter('user'))->countByStatus(1));
    $this->addStats($container, $this->t('Lehrmittel'),
        (new Counter('node', 'lehrmittel'))->countByStatus(1));
    $this->addStats($container, $this->t('Media'),
        (new Counter('media'))->countByStatus(1));
    $this->addStats($container, $this->t('Veranstaltungen'),
        (new Counter('node', 'veranstaltung'))->countByStatus(1));
    $this->addMoreLink($container);
    return $container;
  }

  private function addStats (array &$container, string $key, int $value) {
    $pad_length = 5;
    $container[$key] = [
        '#type' => 'item',
        '#title' => $this->t('@count @key', [
            '@count' => str_pad($value, $pad_length, '_', STR_PAD_LEFT),
            '@key' => $key
        ]),
    ];
  }

  private function addMoreLink (array &$container) {
    $container['more'] = [
        '#type' => 'more_link',
        '#title' => $this->t('Details'),
        '#url' => Url::fromRoute('abc_stats.detail_page'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags () {
    $tags = parent::getCacheTags();
    $tags = Cache::mergeTags($tags, ['user_list', 'node_list', 'media_list']);
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess (AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'view abc stats');
  }

}
