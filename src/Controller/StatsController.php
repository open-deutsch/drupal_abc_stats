<?php

namespace Drupal\abc_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\abc_stats\Counter;
use Drupal\Core\Url;

class StatsController extends ControllerBase {

  public function detailPage () {
    $container = [
        '#type' => 'container',
    ];
    $this->addUserStats($container, new Counter('user'));
    $this->addLehrmittelStats($container, new Counter('node', 'lehrmittel'));
    $this->addMediaStats($container);
    $this->addVeranstaltungStats($container, new Counter('node', 'veranstaltung'));
    $this->addEventDetailLink($container);
    return $container;
  }

  function cmp($a, $b) {
    $date1 = strtotime($a->get('field_veranstaltung_zeit')->value);
    $date2 = strtotime($b->get('field_veranstaltung_zeit')->value);
    if($date1 == $date2) {
      return 0;
    }
    return ($date1 > $date2) ? -1 : 1;
  }

  public function eventdetailPage () {
    $container = [
        '#type' => 'container',
    ];
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'veranstaltung', 'status' => 1]);
    usort($nodes, array($this, "cmp"));
    foreach ($nodes as $node) {
      $this->addEinzelneVeranstaltungStats($container, $node, new Counter('node', 'veranstaltung'));
    }
    return $container;
  }


  private function addVeranstaltungStats (array &$container, Counter $counter) {
    $allRegister = 0;
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => 'veranstaltung', 'status' => 1]);
    $allCountriesArray = array();
    foreach ($nodes as $node) {
      $allRegister += $counter->countRegister($node);
      $allCountriesArray = array_merge($counter->countAllCountries($node), $allCountriesArray);
    }

    $allCountriesCount = count($allCountriesArray);
    $this->addStats($container, $this->t('Veranstaltungen'), [
        'total' => $counter->countAll(),
        'published' => $counter->countByStatus(1),
        'registrations' => $allRegister,
        'countries' => $allCountriesCount,
    ]);
  }

  private function addEinzelneVeranstaltungStats (array &$container, $node, Counter $counter) {
    $countries = $counter->countAllCountries($node);
    $country = json_encode($countries);
    $suchmuster = array("\"", "[", "]", "{", "}", "\\");
    $country = str_replace($suchmuster, '',$country);

    $reasons = $counter->countAllReasons($node);
    $reason = json_encode($reasons);
    $suchmuster = array("\"", "[", "]", "{", "}", "\\");
    $reason = str_replace($suchmuster, '', $reason);
    $dateTime = $node->get('field_veranstaltung_zeit')->value;
    $date = explode("T", $dateTime)[0];
    $date = date("d-m-Y", strtotime($date));

      $this->addStats($container, $node->get('title')->value, [
        $node->get('title')->value => 'Name: ',
        $date => 'Datum: ',
        (new Counter('node', 'veranstaltung'))->countRegister($node) => 'Registrierte Nutzer: ',
        $country => 'Herkunftsländer der Nutzer: ',
        $reason => 'Grund für Teilnahme des Nutzers: ',
    ]);

  }

  private function addUserStats (array &$container, Counter $counter) {
    $this->addStats($container, $this->t('Users'), [
        'total' => $counter->countAll(),
        'active' => $counter->countByStatus(1),
    ]);
  }

  private function addStats (array &$container, string $title, array $elements) {
    $container[$title] = [
        '#type' => 'fieldset',
        '#title' => $title,
    ];
    $pad_length = 5;
    foreach ($elements as $key => $value) {
      $container[$title][$key] = [
          '#type' => 'item',
          '#title' => $this->t('@count @key', [
              '@count' => str_pad($value, $pad_length, '_', STR_PAD_LEFT),
              '@key' => $key
          ]),
      ];
    }
  }

  private function addLehrmittelStats (array &$container, Counter $counter) {
    $this->addStats($container, $this->t('Lehrmittel'), [
        'total' => $counter->countAll(),
        'published' => $counter->countByStatus(1),
    ]);
  }

  private function addMediaStats (array &$container) {
    $this->addStats($container, $this->t('Media'), [
        'total' => (new Counter('media'))->countAll(),
        'Audios' => (new Counter('media', 'audio'))->countAll(),
        'Images' => (new Counter('media', 'image'))->countAll(),
        'H5P' => (new Counter('media', 'h5p_create'))->countAll(),
        'iFrames' => (new Counter('media', 'iframe'))->countAll(),
        'Texts' => (new Counter('media', 'file'))->countAll(),
        'Videos' => (new Counter('media', 'video'))->countAll(),
        'Videos (eingebettet)' => (new Counter('media', 'remote_video'))->countAll(),
    ]);
  }

  private function addEventDetailLink (array &$container) {
    $container['more'] = [
        '#type' => 'more_link',
        '#title' => $this->t('Event Details'),
        '#url' => Url::fromRoute('abc_stats.event_detail_page'),
    ];
  }

}
