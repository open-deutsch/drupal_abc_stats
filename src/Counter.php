<?php

namespace Drupal\abc_stats;

class Counter {

  /**
   * The entity type of this counter.
   *
   * @var string
   */
  protected $entity;

  /**
   * The bundle type of this counter or null for no bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The fieldname used for bundles.
   * 'type' for nodes, but 'bundel' for media.
   *
   * @var string
   */
  private $bundleFieldname;

  /**
   * Constructs a new counter.
   *
   * Note: Currently, bundles might only work for nodes and media.
   *
   * @param string $entity the entity type to be counted.
   * @param string $bundle optional bundle type to be counted (null for no bundle)
   */
  public function __construct (string $entity, string $bundle = null) {
    $this->entity = $entity;
    $this->bundle = $bundle;
    $this->bundleFieldname = strtolower($entity) == 'media' ? 'bundle' : 'type';
  }

  /**
   * Returns the result of counting all entities of the type associated with
   * this counter.
   *
   * @return number|array
   */
  public function countAll () {
    $query = \Drupal::entityQuery($this->entity)->accessCheck(FALSE);
    if (!is_null($this->bundle))
      $query = $query->condition($this->bundleFieldname, $this->bundle);
    return $query->count()->execute();
  }

  /**
   * Returns the result of counting the entities of the type associated with
   * this counter and with the status specified.
   *
   * @param int $status
   * @return number|array
   */
  public function countByStatus (int $status) {
    $query = \Drupal::entityQuery($this->entity)->
        accessCheck(FALSE)->
        condition('status', $status, '=');
    if (!is_null($this->bundle))
      $query = $query->condition($this->bundleFieldname, $this->bundle);
    return $query->count()->execute();
  }

    /**
   * Returns the result of counting the entities of the type associated with
   * this counter and with the status specified.
   *
   * @param array $node
   * @return number|array
   */
  public function countRegister ($node) {
    // $query = \Drupal::service('flag')->getFlaggingUsers($node);
    $title = $node->get('title')->value;
    $query = \Drupal::database()->select('event_mail', 'nfd');
    $query->fields('nfd', ['usercountryorigin']);
    $query->condition('nfd.eventname', $title);
    $num_rows = $query->countQuery()->execute()->fetchField();
    // $counter = count($query);
    return $num_rows;
  }

        /**
   * Returns the result of counting the entities of the type associated with
   * this counter and with the status specified.
   *
   * @param array $node
   * @return array
   */
  public function countAllCountries($node) {
    $title = $node->get('title')->value;

    $query = \Drupal::database()->select('event_mail', 'nfd');
    $query->addField('nfd', 'usercountryorigin');
    $query->condition('nfd.eventname', $title);
    $results = $query->execute()->fetchAll();
    $country = '';
    $countries = array();
    foreach($results as $result) {
      $array_results = (array) $result->usercountryorigin;
      $array_results = json_encode($array_results);
      if(array_key_exists($array_results, $countries)) {
        $countryCount = $countries[$array_results];
        $countryCount ++;
        $countries[$array_results] = $countryCount;
      } else {
        $countries[$array_results] = 1;
      }
    }
    return $countries;
  }


        /**
   * Returns the result of counting the entities of the type associated with
   * this counter and with the status specified.
   *
   * @param array $node
   * @return array
   */
  public function countAllReasons($node) {
    $title = $node->get('title')->value;
    $query = \Drupal::database()->select('event_mail', 'nfd');
    $query->addField('nfd', 'userreason');
    $query->condition('nfd.eventname', $title);
    $results = $query->execute()->fetchAll();
    $reason = '';
    $reasons = array();
    foreach($results as $result) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(intval($result->userreason));
      $name = $term->getName();
      if(array_key_exists($name, $reasons)) {
        $reasonCount = $reasons[$name];
        $reasonCount ++;
        $reasons[$name] = $reasonCount;
      } else {
        $reasons[$name] = 1;
      }
    }
    return $reasons;
  }

      /**
   * Returns the result of counting the entities of the type associated with
   * this counter and with the status specified.
   *
   * @param array $node
   * @return number|array
   */
  public function countOrigin ($node) {
    $title = $node->get('title')->value;
    $query = \Drupal::database()->select('event_mail', 'nfd');
    $query->fields('nfd', ['usercountryorigin']);
    $query->condition('nfd.eventname', $title);
    $result = $query->execute()->fetchAll();
    $result = count($result);
    return $result;
  }

}
