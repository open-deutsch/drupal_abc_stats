# Open Deutsch Statistics

Shows some statistics.

_Note: In the current form this module is only useful for the Open Deutsch website since it processes node bundles and media types that very likely are not present on other sites. But it can be generalized (i.e., made configurable with respect to the data types) in a future version or used as a template for now._

## Licence

Open Deutsch Statistics

Copyright 2019 Hans-Hermann Bode

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
